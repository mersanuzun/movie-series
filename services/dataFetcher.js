const axios = require("axios");
const dataUrl = "https://raw.githubusercontent.com/StreamCo/react-coding-challenge/master/feed/sample.json";

const fetch = async (type) => {
    const response = await axios.get(dataUrl);

    if (response.status !== 200) {
        return Promise.reject(new Error("An error occurred while fetching data"));
    }


    return response.data.entries.reduce((result, current) => {
        if (current.programType === type) {
            if (current.releaseYear >= 2010) return result.concat(current);
            else return result;
        }
        else return result;
    }, []).sort((first, sec) => {
        if (first.title < sec.title)
            return -1;
        if (first.title > sec.title)
            return 1;
        return 0;
    }).slice(0, 21)
}


module.exports = {
    fetchMovies: () => fetch("movie"),
    fetchSeries: () => fetch("series")
}