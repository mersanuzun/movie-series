const express = require("express");
const apiRoutes = require("./routes/apiRoute");

const app = express();

// set routes
apiRoutes(app)

const port = process.env.PORT || 5000;
app.listen(port);