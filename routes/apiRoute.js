const dataFetcher = require("../services/dataFetcher");

const withErrorHandling = async (res, action) => {
    try {
        res.send(
           await action() 
        );
    } catch (e) {
        res.status(503).send({
            error: e.message || "Something went wrong!"
        })
    }
}

module.exports = (app) => {
    app.get("/api/search", async (req, res) => {
        const query = req.query;

        if (!query.type) {
            return res.status(400).send({error: "Not found 'type' parameter"})
        }

        if (query.type === "movie") {
            withErrorHandling(res, dataFetcher.fetchMovies);
        } else if (query.type === "series") {
            withErrorHandling(res, dataFetcher.fetchSeries);
        } else {
            res.status(400).send({
                "error": "Unknown 'type'"
            });
        }
    });
}