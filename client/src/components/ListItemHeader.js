import React from 'react';

import './ListItemHeader.css';

export default (props) => (
    <div className="list-item-header-wrapper">
        <div className="container header">{props.header}</div>
    </div>
);