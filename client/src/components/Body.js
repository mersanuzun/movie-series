import React from 'react';

import ListItemHeader from './ListItemHeader';
import ListItem from './ListItem';
import Loading from './Loading';
import placeHolder from '../assets/placeholder.png';
import { connect } from 'react-redux';
import { watchingTypes } from '../reducers/watchingReducer';
import * as types from '../actions';

import './Body.css';

class Body extends React.Component {

    generateBody(props) {
        switch (props.selectionType) {
            case watchingTypes.ALL:
                return (
                    <ListItem
                        items={
                            Object.keys(watchingTypes)
                                .slice(1)
                                .map(type => {
                                    return {
                                        key: type,
                                        imageUrl: placeHolder,
                                        title: watchingTypes[type].display,
                                        onItemClick: () => this.props.selectType(type)
                                    }
                                })
                        }
                    />
                )
            case watchingTypes.MOVIE:
                return (
                    <ListItem
                        fetchType="movie"
                        items={
                            props.watchings.map(item => {
                                return {
                                    key: item.title + item.releaseYear,
                                    title: item.title,
                                    imageUrl: item.images["Poster Art"].url || placeHolder
                                }
                            })
                        }
                    />
                )
            case watchingTypes.SERIES:
                return (
                    <ListItem
                        fetchType="series"
                        items={
                            props.watchings.map(item => {
                                return {
                                    key: item.title + item.releaseYear,
                                    title: item.title,
                                    imageUrl: item.images["Poster Art"].url || placeHolder
                                }
                            })
                        }
                    />
                )
            default:
                return "Bilinmeyen tür.";
        }
    }

    render() {
        return (
            <div className={this.props.isFetching || this.props.error ? "body-centered" : "body-wrapper"}>
                {
                    this.props.isFetching ? (<Loading />) : (
                        this.props.error ? (
                            <div className="alert alert-danger">{this.props.error.error}</div>
                        ) : (
                            <div>
                                <ListItemHeader header={this.props.selectionType.display} />
                                <div className="body container">
                                    {
                                        this.generateBody(this.props)
                                    }
                                </div>
                            </div>
                        )
                    )
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        selectionType: state.watching.selectionType,
        watchings: state.watching.data,
        isFetching: state.watching.isFetching,
        error: state.watching.error
    }
}

export default connect(mapStateToProps, types)(Body);