import React from 'react';
import Header from './Header';
import Body from './Body';
import Footer from './Footer';
import Loading from './Loading';

import './App.css';

export default class App extends React.Component {
  render() {
    return (
      <div className="app">
        <Header />
        <Body />
        <Footer />
      </div>
    )
  }
}