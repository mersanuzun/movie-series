import React from 'react';

import './Header.css';
import { connect } from 'react-redux';
import * as actions from '../actions';

class Header extends React.Component {
    render() {
        return (
            <header>
                <div className="navbar">
                    <div className="container d-flex justify-content-between">
                        <a
                            href="#"
                            className="navbar-brand d-flex align-items-center"
                            onClick={() => this.props.selectType("ALL")}
                        >
                            <strong className="home">Demo Streaming</strong>
                        </a>
                        <div className="header-right">
                            <a href="#">Login</a>
                            <a href="#" className="start-free-btn">Start your free trial.</a>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}

export default connect(null, actions)(Header)