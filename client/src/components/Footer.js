import React from 'react';

import './Footer.css';
import facebook from '../assets/facebook-white.svg';
import twitter from '../assets/twitter-white.svg';
import instagram from '../assets/instagram-white.svg';
import appStore from '../assets/app-store.svg';
import playStore from '../assets/play-store.svg';
import windowsStore from '../assets/windows-store.svg';

export default () => (
    <footer className="py-5">
        <div className="container">
            <div className="row">
                <div className="col-12 col-md">
                    Home | Terms and Conditions | Privacy Polices | Collection Statement | Help | Manage Account
                </div>
            </div>
            <div className="row">
                <div className="col-12 col-md">
                    Copyright &copy; 2018 Demo Streaming All Rights Reserved.
                </div>
            </div>
            <div className="row">
                <div className="col-12 col-md link-wrapper">
                    <div className='social-wrapper'>
                        <a href="http://www.facebook.com" target="blank">
                            <img className="social facebook" src={facebook} />
                        </a>
                        <a href="http://www.twitter.com" target="blank"> 
                        <img className="social twitter" src={twitter} />
                        </a>
                        <a href="http://www.instagram.com" target="blank">
                        <img className="social instagram" src={instagram} />
                        </a>
                    </div>
                    <div className="download-wrapper">
                        <img className="download" src={appStore} />
                        <img className="download" src={playStore} />
                        <img className="download" src={windowsStore} />
                    </div>
                </div>
            </div>
        </div>
    </footer>
)