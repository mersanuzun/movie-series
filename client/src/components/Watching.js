import React from 'react';
import ReactTooltip from 'react-tooltip';

import './Watching.css';

export default (props) => (
    <div data-tip={props.title}>
        <div
            className="watching"
            onClick={props.onItemClick || null}
        >
            <div className="poster-wrapper">
                <img
                    className="poster"
                    src={props.imageUrl}
                    alt={"Poster"}
                />
            </div>
            <div className="title-wrapper">
                <span className="title">{props.title}</span>
            </div>
        </div>
        <ReactTooltip place="top" type="dark" effect="float" />
    </div>
)