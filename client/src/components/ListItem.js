import React from 'react';

import ListItemHeader from './ListItemHeader';
import Watching from './Watching';
import placeHolder from '../assets/placeholder.png';
import { connect } from 'react-redux';
import { watchingTypes } from '../reducers/watchingReducer';
import * as actions from '../actions';

import './ListItem.css';

class ListItem extends React.Component {

    generateItems(items) {
        return items.map(item => (
            <Watching
                key={item.key}
                imageUrl={item.imageUrl}
                title={item.title}
                onItemClick={item.onItemClick}
            />
        ))
    }

    componentDidUpdate() {
        if (this.props.fetchType && this.props.items.length === 0) {
            this.props.fetchWatchings(this.props.fetchType)
        }
    }

    render() {
        return (
            <div className="list-item-wrapper">
                {
                    this.generateItems(this.props.items)
                }
            </div>
        )
    }
}


export default connect(null, actions)(ListItem);