import * as types from '../actions/types';
import axios from "axios";

export const selectType = (type) => {
    return {
        type: types.SELECT_WATCHING,
        data: type
    }
}

export const fetchWatchings = (type) => {
    return async (dispatch) => {
        dispatch({
            'type': types.FETCH_WATCHING
        });

        try {
            const response = await axios.get("/api/search", {
                params: {
                    'type': type
                }
            });

            dispatch({
                'type': types.FETCHED_WATCHING,
                'data': response.data
            });
        } catch (err) {
            dispatch({
                'type': types.ERROR_DURING_FETCHING,
                'data': err.response.data
            })
        }
    }
}