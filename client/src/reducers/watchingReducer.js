import * as types from '../actions/types';

export const watchingTypes = {
    ALL: {
        key: "all",
        display: "Popular Titles"
    },
    SERIES: {
        key: "series",
        display: "Popular Series"
    },
    MOVIE: {
        key: "movie",
        display: "Popular Movies"
    }
}

const initalStore = {
    selectionType: watchingTypes.ALL,
    data: [],
    isFetching: false,
    error: null
}

export default (store = initalStore, action) => {
    switch (action.type) {
        case types.SELECT_WATCHING:
            return {
                ...store,
                data: [],
                error: null,
                selectionType: watchingTypes[action.data]
            }
        case types.FETCH_WATCHING:
            return {
                ...store,
                isFetching: true
            }
        case types.FETCHED_WATCHING:
            return {
                ...store,
                isFetching: false,
                data: action.data,
                error: null
            }
        case types.ERROR_DURING_FETCHING:
            return {
                ...store,
                isFetching: false,
                error: action.data
            }
        default:
            return store
    }
} 