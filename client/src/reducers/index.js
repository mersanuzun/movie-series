import { combineReducers } from "redux";
import watchingReducer from "./watchingReducer.js";

export default combineReducers(
    {
        watching: watchingReducer 
    }
)